# Recupera archivos .png de un archivo de memoria .bin

import struct
import os


def pngcopy(infile, outfile):
    # copy header
    header = infile.read(8)
    if header != b'\x89PNG\r\n\x1a\n':
        print("not a valid PNG file")
        return

    outfile.write(header)

    # copy chunks, until IEND
    while 1:
        chunk = infile.read(8)
        size, cid = struct.unpack("!l4s", chunk)
        outfile.write(chunk)
        outfile.write(infile.read(size))
        outfile.write(infile.read(4))  # checksum
        if cid == b'IEND':
            break


def main():
    if not os.path.exists("recovery"):
        os.makedirs("recovery")

    if not os.path.exists("recovery/png"):
        os.makedirs("recovery/png")

    count = 0
    for arc in range(10, 11):
        print("begin reading parts/input%s.bin" % str(arc))
        infile = open("parts/input%s.bin" % str(arc), "rb")
        content = infile.read()
        print("end reading parts/input%s.bin" % str(arc))
        length = len(content)
        index = 0
        while index < length:
            i = content.find(b'\x89PNG\r\n\x1a\n', index)
            if i == -1:
                break
            count += 1
            print("png file found")
            outfile = open("recovery/png/image%s.png" % str(count), "wb")
            infile.seek(i)
            pngcopy(infile, outfile)
            outfile.close()
            index = i + 1

        infile.close()

    print("Finished.")
    print("PNG Files Found: " + str(count))
    input()


if __name__ == "__main__":
    main()
