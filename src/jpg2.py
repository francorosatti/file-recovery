# recupera archivos .jpg de Nikon D3200 desde un archivo de memoria .bin

import struct
import os


def jpgcopy(infile, outfile):
    # copy header
    header = infile.read(10)
    if header != b'\xFF\xD8\xFF\xE1\xFF\xFE\x45\x78\x69\x66':
        print("not a valid JPG file")
        return

    outfile.write(header)

    # copy chunks, until IEND
    while 1:
        chunk = infile.read(4)
        ff, cid, size = struct.unpack("!BBH", chunk)

        if ff != 0xFF:
            break

        if cid == 0xDA:
            while 1:
                ff = infile.read(2)
                print(ff)
                outfile.write(ff)
                if ff == 0xff:
                    cid = infile.read(2)
                    outfile.write(cid)
                    if cid == 0xD9:
                        print("END")
                        break

        outfile.write(chunk)
        outfile.write(infile.read(max(0, size-2)))
        if cid == 0xD9:
            break


def main():
    if not os.path.exists("recovery"):
        os.makedirs("recovery")

    count = 0
    for arc in range(1, 15):
        if not os.path.exists("recovery/jpg%s" % str(arc)):
            os.makedirs("recovery/jpg%s" % str(arc))
        print("begin reading parts/input%s.bin" % str(arc))
        infile = open("parts/input%s.bin" % str(arc), "rb")
        content = infile.read()
        print("end reading parts/input%s.bin" % str(arc))
        length = len(content)
        index = content.find(b'\xFF\xD8\xFF\xE1\xFF\xFE\x45\x78\x69\x66', 0)
        if index != -1:
            while index < length:
                i = content.find(b'\xFF\xD8\xFF\xE1\xFF\xFE\x45\x78\x69\x66', index)
                if i == -1:
                    break
                count += 1
                print("jpg file found")
                outfile = open("recovery/jpg%s/image%s.jpg" % (str(arc), str(count)), "wb")
                infile.seek(index - 1)
                outfile.write(infile.read(i - index + 1))
                # print("INDEX: " + str(hex(i)))
                # jpgcopy(infile, outfile)
                outfile.close()
                index = i + 1

        infile.close()

    print("Finished")
    print("JPG Files Found: " + str(count))
    input()


if __name__ == "__main__":
    main()
