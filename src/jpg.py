# pruebas de recuperacion de archivos jpg

import struct
import os


def jpgcopy(infile, outfile):
    # copy header
    header = infile.read(2)
    if header != b'\xFF\xD8':
        print("not a valid JPG file")
        return

    outfile.write(header)

    # copy chunks, until IEND
    while 1:
        chunk = infile.read(4)
        ff, cid, size = struct.unpack("!BBH", chunk)

        if ff != 0xFF:
            break

        if cid == 0xDA:
            while 1:
                ff = infile.read(2)
                print(ff)
                outfile.write(ff)
                if ff == 0xff:
                    cid = infile.read(2)
                    outfile.write(cid)
                    if cid == 0xD9:
                        print("END")
                        break

        outfile.write(chunk)
        outfile.write(infile.read(max(0, size-2)))
        if cid == 0xD9:
            break


def main():
    if not os.path.exists("recovery"):
        os.makedirs("recovery")

    if not os.path.exists("recovery/test"):
        os.makedirs("recovery/test")

    count = 0
    for arc in range(1, 15):
        print("begin reading parts/input%s.bin" % str(arc))
        infile = open("parts/input%s.bin" % str(arc), "rb")
        content = infile.read()
        print("end reading parts/input%s.bin" % str(arc))
        length = len(content)
        index = 0
        while index < length:
            i = content.find(b'\xFF\xD8\xFF\xE0\x00\x10\x4A\x46\x49\x46', index)
            if i == -1:
                break
            count += 1
            print("jpg file found")
            # outfile = open("recovery/image%s.jpg" % str(count), "wb")
            # infile.seek(i)
            # print("INDEX: " + str(hex(i)))
            # jpgcopy(infile, outfile)
            # outfile.close()
            index = i + 1

        infile.close()

    print("Finished")
    print("JPG Files Found: " + str(count))
    input()


if __name__ == "__main__":
    main()
