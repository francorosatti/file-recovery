# Fragmenta un archivo .bin

import os


def main():
    if not os.path.exists("parts"):
        os.makedirs("parts")

    infile = open("memory.bin", "rb")

    block_sz = 0x40000000
    for i in range(16):
        print("part%s" % str(i))
        outfile = open("parts/input%s.bin" % str(i), "wb")
        print("reading content")
        content = infile.read(block_sz)
        print("writing content")
        outfile.write(content)
        outfile.close()

    infile.close()


if __name__ == "__main__":
    main()
